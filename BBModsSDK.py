# -*- coding: utf-8 -*-

import BigWorld, datetime, urllib, urllib2, json, re, threading, os, traceback, constants
from Queue import Queue
from helpers import dependency
from ResMgr import openSection
from helpers import getLanguageCode
from gui import SystemMessages as SM
from PlayerEvents import g_playerEvents
from skeletons.connection_mgr import IConnectionManager
from skeletons.gui.shared import IItemsCache
from skeletons.gui.battle_session import IBattleSessionProvider
from skeletons.account_helpers.settings_core import ISettingsCore

connectionManager = dependency.instance(IConnectionManager)
g_sessionProvider = dependency.instance(IBattleSessionProvider)
g_itemsCache = dependency.instance(IItemsCache)
settingsCore = dependency.instance(ISettingsCore)


SDK_Ver = '3.4'
WoT_Ver = BigWorld.wg_getProductVersion()

Language = getLanguageCode()

def Cap(*args, **kwargs):
	return

def CapNone(*args, **kwargs):
	return None

def Paths2res_mods():
	res = openSection('../paths.xml')['Paths'].values()
	for path in res:
		if 'res_mods' in path.asString: return path.asString, path.asString.replace('res_mods', 'mods')

ResMods = Paths2res_mods()


class LoadConfig(object):
	def __init__(self, name, File = None, Path = None):
		self.name = name
		self.File = File if File else '%s.json' % name
		self.DefPath = 'mods/configs/BBMods/' if os.path.exists('mods/configs/BBMods/%s' % self.File) else 'res_mods/configs/BBMods/'
		self.Path = Path if Path else self.DefPath
		self.SubPath = '%s%s/' % (self.Path, self.name)
		self.ConfigFiles = [self.Path + self.File]
		self.p = re.compile('\${"\w*/?\w*\.json"\}')
		self.LoadConfig()

	def LoadParse(self, File):
		Comment = False
		response = ''
		Config = {}
		conf = []
		try: response = open(File, 'r').read().decode('utf8')
		except:
			self.NotValidConfig = True
			Errors.ErrorQueue.put(('None', self.name, str(File)))
			BigWorld.logWarning(self.name, 'The configuration file: %s is unavailable or corrupted. Will use the default settings!' % str(File), None)
		if response:
			for line in response.split('\n'):
				if '/*' in line:
					Comment = True
					continue
				if '*/' in line:
					Comment = False
					continue
				if Comment: continue
				line = line.split('// ')[0]
				line = line.strip()
				if line: conf.append(line)
			response = ''.join(conf)
			for f in self.p.findall(response):
				self.ConfigFiles.append(self.Path + f.replace('${"', '').replace('"}', ''))
				response = response.replace(f, '"False":0')
		try: Config = json.loads(response)
		except Exception as error:
			Errors.ErrorQueue.put(('Error', self.name, str(File)))
			self.NotValidConfig = True
			msg = ''
			temp = error.message.split(': ')
			if len(temp) > 1:
				num = int(re.search('(char \d*)', temp[1]).group().replace('char ', ''))
				msg = ' in code `%s`' % response[num-20:num+20]
			BigWorld.logWarning(self.name, 'Error in the JSON configuration file %s! Error: `%s`%s' % (str(File), temp[0], str(msg)), None)
		return Config

	def ConfigAssociation(self, data):
		for i in data:
			if type(data[i]) is dict and self.Config.get(i): self.Config[i].update(data[i])
			else: self.Config[i] = data[i]

	def LoadConfig(self):
		self.Config = {}
		self.NotValidConfig = False
		while self.ConfigFiles:
			ConfigFile = self.ConfigFiles.pop(0)
			self.ConfigAssociation(self.LoadParse(ConfigFile))
		if os.path.exists(self.SubPath):
			FList = filter(lambda i: '.json' in i, os.listdir(self.SubPath))
			for f in FList:
				self.ConfigAssociation(self.LoadParse('%s%s' % (self.SubPath, f)))

	def getConfig(self):
		return self.Config

	def ReLoadConfig(self):
		self.LoadConfig()
		return self.Config

	def isValidConfig(self):
		return not self.NotValidConfig


class Analytics(object):
	def __init__(self, tid, name, ver, config = 'default'):
		self.tid = tid
		self.name = name
		self.ver = '%s SDK:%s' % (ver, SDK_Ver)
		self.config = config
		self.lang = Language.upper()
		connectionManager.onLoggedOn += self.Start
		connectionManager.onDisconnected += self.End

	def Send(self, mode = 'start'):
		url = 'http://www.google-analytics.com/collect'
		param = urllib.urlencode({
			'sc': mode,
			'v': 1,
			'tid': self.tid,
			'cid': self.ID,
			't': 'screenview',
			'an': self.name,
			'av': self.ver,
			'cd': '%s-%s' % (constants.AUTH_REALM, self.lang),
			'ul': self.lang,
			'aid': self.config
		})
		try:
			req = urllib2.Request(url, param, headers={'User-Agent': '%s/%s' % (self.name, self.ver)})
			resp = urllib2.urlopen(req, timeout=3).read()
		except:
			BigWorld.logWarning(self.name, 'Unable to send data analytics', None)

	def Start(self, data):
		self.ID = data['token2'].split(':')[0]
		threading.Thread(target = self.Send).start()

	def End(self):
		self.Send('end')


class EventHook(object):
	def __init__(self, event, pre):
		self.event = event
		self.pre = pre

	def __call__(self, *args):
		if not self.pre(*args): return
		self.event(*args)
		return

	def __iadd__(self, delegate):
		self.event += delegate
		return self

	def __isub__(self, delegate):
		self.event -= delegate
		return self

	def clear(self):
		self.event.clear()

	def __repr__(self):
		return repr(self.event)


class ModLogging(object):
	def __init__(self, Name, ErrorLevel, File = None, Path = None):
		Path = Path if Path else 'logs/'
		self.LogData = Queue()
		self.Name = Name
		self.File = '%s%s.log' % (Path, Name)

		self.Level = ErrorLevel
		if self.Level != 0:
			self.thread = threading.Thread(target = self.WriteData)
			self.thread.setDaemon(True)
			self.thread.setName('Logging')
			self.thread.start()
			self.LogData.put(('', '\n\n*--------------------------- Начата новая игровая сессия ---------------------------*', None, None))

	def Add(self, Level1, Level2 = None, Level3 = []):
		if self.Level != 0: self.LogData.put((datetime.datetime.now().isoformat(sep=' '), Level1, Level2, Level3))

	def WriteData(self):
		while True:
			Time, Level1, Level2, Level3 = self.LogData.get()
			logfile = open(self.File, 'a')
			try:
				logfile.write('%s: %s\n' % (Time, Level1))
				if self.Level >= 2 and Level2: logfile.write('  %s\n' % Level2)
				if self.Level == 3 and Level3:
					for i in Level3: logfile.write('  %s: %s\n' % (i, Level3[i]))
			except:
				BigWorld.logWarning(self.Name, 'Ошибка записи данных в лог', None)
				BigWorld.logWarning(self.Name, traceback.format_exc(limit=10), None)
			logfile.close()


class ErrorWarning(object):
	def __init__(self):
		self.ErrorMsg = {
			'None': {
				'ru': 'Конфигурационный файл <font color="#D3D3D3">%s</font> мода <font color="#D3D3D3">"%s"</font> не найден',
				'en': 'The configuration file <font color="#D3D3D3">%s</font> for mod <font color="#D3D3D3">%s</font> not found'
			},
			'Error': {
				'ru': 'в конфигурационном файле <font color="#D3D3D3">%s</font> мода <font color="#D3D3D3">"%s"</font>',
				'en': 'in the configuration file <font color="#D3D3D3">%s</font> for mod <font color="#D3D3D3">%s</font>'
			}
		}
		self.ErrorQueue = Queue()
		self.Lang = Language if Language == 'ru' else 'en'
		self.inHangar = threading.Event()
		g_playerEvents.onAccountShowGUI += self.onAccountShowGUI
		g_playerEvents.onAvatarBecomePlayer += self.onAvatarBecomePlayer

		self.thread = threading.Thread(target = self.ShowError)
		self.thread.setDaemon(True)
		self.thread.setName('ShowError')
		self.thread.start()

	def onAccountShowGUI(self, ctx):
		self.inHangar.set()

	def onAvatarBecomePlayer(self):
		self.inHangar.clear()

	def ShowError(self):
		while True:
			Code, ModName, Data = self.ErrorQueue.get()
			self.inHangar.wait()
			ErrorMsg = self.ErrorMsg[Code][self.Lang]
			SM.pushMessage(ErrorMsg % (Data, ModName), SM.SM_TYPE.Error)

Errors = ErrorWarning()

#class FunctionHook(object):
#	def __init__(self, func, pre = None, post = None):
#		self.func = func
#		self.pre = pre
#		self.post = post

#	def __call__(self, *args):
#		if self.pre:
#			result = self.pre(*args)
#			if result: return result
#		self.result = self.func(*args)
#		if self.post: self.post(*args)
#		return self.result
